import React,{useContext} from 'react'
import {CategoryContext} from '../../context/categoryContext'

function Heading(props) {
    
    const [context, setContext] = useContext(CategoryContext) 
    function setActivity(event){
        let categoryItem = document.querySelectorAll('.heading-list-item')
        context.postFetching(event)
        for (let i = 0; i < categoryItem.length; i++) {
            if(event===categoryItem[i].getAttribute('value')){
                for (let j = 0; j < categoryItem.length; j++) {
                    categoryItem[j].classList.remove('active')
                }
                categoryItem[i].classList.add('active')
                
            }else{
                categoryItem[i].classList.remove('active')
            }
        }

       
    }

    return (
        <div className='heading'>
        <h1 className='heading-h1'>Recent Posts</h1>
            <ul className='heading-list'>
                <li className='heading-list-item heading-list-item-icon'><img src='./assets/images/down.png' /></li>
                <div className='heading-list-container'>
                <li onClick={()=>{setActivity('all')}} value='all' className='heading-list-item active'>All</li>
                {context.categories && context.categories.map((category,index)=>{
                    return <li key={index} onClick={()=>{setActivity(category.title)}} value={category.title} className='heading-list-item'>{category.title}</li>
                })}
                </div>
            </ul>
        </div>
    )
}

  

export default Heading
