import React from 'react'
import Link from 'next/link'

function BlogCard(props) {
    let date = new Date(props.data._createdAt)
    let month = date.getMonth()
    let day = date.getDay()
    let year = date.getFullYear()
    let months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
        ];
    return (
        <div className='blogCard'>
            <div className='blogCard_content'> 
                <span className='blogCard_time'>{months[month]} {day}.{year}</span>
                <h1 className='blogCard_head'>
                    <Link href={`/post/${props.data.slug.current}`}>
                        {props.data.title}
                    </Link>
                </h1>
                <div className='blogCard_keywords'>
                {props.data.keyword.map((key,idx)=>{
                    return <span key={idx} className='hashtag'>#{key}</span>
                })}
                </div>
            </div>
        </div>
    )
}

export default BlogCard
