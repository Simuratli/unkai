import React,{useEffect} from 'react'
// import Link from 'next/link'

function Footer() {
    function scrollTOTop() { 
        window.scroll(0,0)
     }

    return (
        <footer className='footer'>
            <span onClick={scrollTOTop} className='scroll-top' ><img alt="scroll" src={`/assets/images/up.png`} /></span>
            <div className='container'>
                <div className='footer_content'>
                    <h1 className='footer_text'>Copyright &copy; 2020 Unkai, Inc</h1>
                    {/* <div className='share'>
                        <Link href='https://www.facebook.com/Unkai-Studio-101847372059104' className='share-img'><img alt="facebook" src='/assets/icons/facebook.png' /></Link>
                        <Link href='https://www.youtube.com/channel/UCkCJne-ETCwxZJhgjo-1IHQ' className='share-img'><img alt="youtube" src='/assets/icons/youtube.png' /></Link>
                        <Link href='https://www.instagram.com/unkai.studio/' className='share-img'><img alt="instagram" src='/assets/icons/instagram.png' /></Link>
                    </div> */}
                </div>
            </div>
        </footer>
    )
}

export default Footer
