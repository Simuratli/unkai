import React,{useState,useContext,useEffect} from 'react'
import { useRouter } from "next/router";
import {CategoryContext} from '../../context/categoryContext'

function Navbar(props) {
    const router = useRouter();
    const [sidebar, setSidebar] = useState(false);
    const [search, setSearch] = useState('')
    const [context, setContext] = useContext(CategoryContext)
    const [hamburger, setHamburger] = useState('menu')
    
   

    function hamburgerMenu() { 
        setSidebar(!sidebar)
        if(!sidebar){
            setHamburger('x')
        }else{
            setHamburger('menu')
        }
    }

    function handleChange(e){
        const {name,value} = e.target
        setSearch(value)
    }

    function handleSubmit(e){
        e.preventDefault()
        router.push(`/search/${search}`)
        context.search(search)
    }

    return (
        <div className='container'>
            <nav className='navbar'>
                <div className='navbar_logo'>
                    <h1><a href='/'>Unkai</a></h1>
                </div>
                <div className='navbar_items'>
                    <div className={`navbar_navigation ${sidebar && 'open'}`} >
                        <a className={`nav-link ${router.pathname == "/" && "active" }`} href='/'>Home</a>
                        <a className={`nav-link ${router.pathname == "/blog" && "active" }`} href='/blog'>Blog</a>
                        <a className={`nav-link ${router.pathname == "/contact" && "active" }`} href='/contact'>Contact Us</a>
                    </div>
                    <div className='navbar_search'>
                        <form onSubmit={handleSubmit}>
                            <input value={search} onChange={handleChange} autoComplete='off' placeholder='Search' />
                            <button><img alt="search" src='/assets/images/search.png' /></button>
                        </form>
                    </div>
                </div>
            </nav>
            <div  className='hamburger'>
                <img alt='hamburger' onClick={hamburgerMenu} src={`/assets/images/${hamburger}.png`} />
            </div>
        </div>
    )
}

export default Navbar
