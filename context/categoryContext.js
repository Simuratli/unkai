import React,{useState,useEffect} from 'react'
import {createContext} from 'react'
import {sanityClient} from '../sanity'
export const CategoryContext = createContext([1,23,54,23])

export const CategoryProvider = props =>{
    const [search, setSearch] = useState('')
    const [thm, setThm] = useState('dark')
    const fetchSearch = async (event) =>{
        setSearch(event)
    }    
    const fetchPosts = async (event) =>{
        setCategory((prev)=>({
            ...prev,
            loading:true
        }))
        let query 
        if(event === 'all'){
            query = `*[_type=="post"]{
                ...,
                "categories": categories[]->title,
            }`
        }else {
            query = `*[_type=="post" && references( *[_type=="category" && title == "${event}"]._id ) ]{
                ...,
                "categories": categories[]->title,
            }`
        }
        const properties = await sanityClient.fetch(query)
        setCategory((prev)=>({
            ...prev,
            loading:false
        }))
        setCategory((prev)=>({
            ...prev,
            posts:properties
        }))
    }

    const setTheme = async (event) =>{
        let storage = localStorage.getItem('theme')
        setThm(event)
    }

    const fetch = async ()=>{
        setCategory((prev)=>({
            ...prev,
            loading:true
        }))
        const queryCategories = '*[ _type == "category"]'
        const ctg = await sanityClient.fetch(queryCategories)
        setCategory((prev)=>({
            ...prev,
            loading:false
        }))
        const query = `*[ _type == "post"]{
            ...,
            "categories": categories[]->title,
        }`
        const properties = await sanityClient.fetch(query)
        setCategory((previous)=>({
            ...previous,
            categories:ctg,
            posts:properties
        }))
    }
    useEffect(() => {
        fetch()
    }, [])

    const [category, setCategory] = useState({
        categories:null,
        posts:null,
        postFetching:fetchPosts,
        loading:false,
        search:fetchSearch,
        theme:setTheme,
        thm:thm
    })

    return <CategoryContext.Provider value={[category, setCategory]}>
        {props.children}
    </CategoryContext.Provider>
}


