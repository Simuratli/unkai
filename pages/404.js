import React from 'react'
import Link  from 'next/link'
import Head from 'next/head'

function NotFound() {
    return (
        <div className='notFound'>
         <Head>
            <title>Unkai || 404</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta charSet="UTF-8"/>
            <meta name="description" content="Unkai studio 404 not found page"/>
            <meta name="keywords" content="HTML, CSS, JavaScript, Unkai 404"/>
            <meta name="author" content="Eljan Simuratli"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </Head>
        <Head>
            <meta property="og:title" content="Unkai 404 Page"/>
            <meta property="og:description" content="Unkai studio 404 not found page"/>
            <meta property="og:image" content="https://i.resimyukle.xyz/ARHRRK.png"/>
            <meta property="og:url" content="http://unkai.vercel.app/404"/>
            <meta name="twitter:title" content="Unkai studio 404 not found page "/>
            <meta name="twitter:description" content=" Unkai studio 404 not found page"/>
            <meta name="twitter:image" content="https://i.resimyukle.xyz/ARHRRK.png"/>
        </Head>
            <div className='notFound-content'>
                <div className='notFound-img'>
                    <img src='/assets/images/notfound.png' />
                </div>
                <h1 className='notFound-head'>Page not found - notFound</h1>
                <p className='notFound-text'>This page not found (deleted or never exists).
                    <br/>
                    Try a phrase in search box or back to home and start again.
                </p>
                <div className='notFound-link-container'>
                    <Link href='/' className='notFound-link'>
                        TAKE ME HOME!
                    </Link>
                    <img src='/assets/images/down.png' />
                </div>
            </div>sa
        </div>
    )
}

export default NotFound
