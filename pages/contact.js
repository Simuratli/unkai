import React from 'react'
import Head from 'next/head'

function Contact() {
    return (
        <div className='container'>
        <Head>
            <title>Unkai || Contact</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta charSet="UTF-8"/>
            <meta name="description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics. Contact With Unkai"/>
            <meta name="keywords" content="HTML, Unkai contact CSS, JavaScript, Unkai 404 , game development , topics about different technologies , Unity , Unkai studio, Japan game studio , Azerbaijan game studio, Azerbaijan tech , tech , game studio"/>
            <meta name="author" content="Eljan Simuratli"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </Head>
        <Head>
            <meta property="og:title" content="Unkai || Contact"/>
            <meta property="og:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics. Contact with Unkai"/>
            <meta property="og:image" content="https://i.resimyukle.xyz/8Ba0B7.png"/>
            <meta property="og:url" content="http://unkai.vercel.app/blog"/>
            <meta name="twitter:title" content="Unkai || Contact"/>
            <meta name="twitter:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="twitter:image" content="https://i.resimyukle.xyz/8Ba0B7.png"/>
        </Head>
            <div className='contact'>
                <form className='contact-form'>
                    <div className='form-flex'>
                        <div className='form-group'>
                            <label>Name*</label>
                            <input placeholder='Shane Hobbins' />
                        </div>
                        <div className='form-group'>
                            <label>Email*</label>
                            <input placeholder='info@interface.com' />
                        </div>
                    </div>
                    <div className='form-group textarea-form'>
                        <label>Message*</label>
                        <textarea placeholder='Hi there' />
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Contact
