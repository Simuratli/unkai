import { SitemapStream, streamToPromise } from 'sitemap';
import {sanityClient} from '../../sanity'
export default async (req, res) => {
  try {
    const smStream = new SitemapStream({
      hostname: `https://${req.headers.host}`,
      cacheTime: 600000,
    });

    let query = `*[_type=="post"]{
        slug,
        _createdAt,
    }`
    const properties = await sanityClient.fetch(query)
    // List of posts
    const posts = [];

    // Create each URL row
    properties.forEach(p => {
      smStream.write({
        url: `/post/${p.slug.current}`,
        changefreq: 'daily',
        priority: 0.9
      });
    });

    // End sitemap stream
    smStream.end();

    // XML sitemap string
    const sitemapOutput = (await streamToPromise(smStream)).toString();

    // Change headers
    res.writeHead(200, {
      'Content-Type': 'application/xml'
    });

    // Display output to user
    res.end(sitemapOutput);
  } catch(e) {
    console.log(e)
    res.send(JSON.stringify(e))
  }

}