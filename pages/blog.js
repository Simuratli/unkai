import React from 'react'
import BlogCard from "../components/BlogCard/BlogCard";
import {sanityClient} from '../sanity'
import Head from 'next/head'
import {CategoryContext} from '../context/categoryContext.js'
import {useContext} from 'react'
import Loader from '../components/Loader'

function Blog(props) {

  const [context, setContext] = useContext(CategoryContext)
  let CARDS = context.posts && context.posts.map((item,index)=>{
    return <BlogCard data={item} key={index} />
  })

  if(context.loading) CARDS = <Loader/>


    return (
        <div className='container'>
        <Head>
            <title>Unkai || Blog</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta charSet="UTF-8"/>
            <meta name="description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="keywords" content="HTML, CSS, JavaScript, Unkai 404 , game development , topics about different technologies , Unity , Unkai studio, Japan game studio , Azerbaijan game studio, Azerbaijan tech , tech , game studio"/>
            <meta name="author" content="Eljan Simuratli"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </Head>
        <Head>
            <meta property="og:title" content="Unkai || Blog"/>
            <meta property="og:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta property="og:image" content="https://i.resimyukle.xyz/fCdM45.png"/>
            <meta property="og:url" content="http://unkai.vercel.app/blog"/>
            <meta name="twitter:title" content="Unkai || Blog"/>
            <meta name="twitter:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="twitter:image" content="https://i.resimyukle.xyz/fCdM45.png"/>
        </Head>
            <div className='blog'>
                <div className='blog-categories'>
                {
                    props.category.map((item,index)=>{
                        return <button onClick={()=>{context.postFetching(item.title)}} key={index} className='hashtag'>{item.title}</button>
                    })
                }
                </div>
                <div className='blog-content'>
                {CARDS}
                </div>
            </div>
        </div>
    )
}

export const getServerSideProps = async () => {
    const query = '*[ _type == "post"]'
    const queryCategories = '*[ _type == "category"]'
    const properties = await sanityClient.fetch(query)
    const category = await sanityClient.fetch(queryCategories)
  
    if (!properties.length) {
      return {
        props: {
          properties: [],
          category: [],
        },
      }
    } else {
      return {
        props: {
          properties,
          category
        },
      }
    }
  }

export default Blog
