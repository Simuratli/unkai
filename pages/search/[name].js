import React,{useContext} from 'react'
import {useRouter} from 'next/router'
import {CategoryContext} from '../../context/categoryContext'
import {sanityClient} from '../../sanity'
import BlogCard from "../../components/BlogCard/BlogCard";
import Loader from '../../components/Loader.jsx'
import Head from 'next/head'
function Posts(props) {

    let Cards = props.property ? props.property.map((item,i)=>{
        return <BlogCard data={item} key={i} />
    }) : <div>Empty</div>
    

    const [context, setContext] = useContext(CategoryContext)
    const router = useRouter()
    const { name } = router.query
    return (
        <div className='container'>
        <Head>
            <title>Unkai | {name}</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta charSet="UTF-8"/>
            <meta name="description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="keywords" content="HTML, CSS, JavaScript, Unkai 404 , game development , topics about different technologies , Unity , Unkai studio, Japan game studio , Azerbaijan game studio, Azerbaijan tech , tech , game studio"/>
            <meta name="author" content="Eljan Simuratli"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </Head>
        <Head>
            <meta property="og:title" content="Unkai || Blog"/>
            <meta property="og:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta property="og:image" content="https://i.resimyukle.xyz/fCdM45.png"/>
            <meta property="og:url" content="http://unkai.vercel.app/blog"/>
            <meta name="twitter:title" content="Unkai || Blog"/>
            <meta name="twitter:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="twitter:image" content="https://i.resimyukle.xyz/fCdM45.png"/>
        </Head>
        <div className='blog'>
                <div className='blog-content'>
                {Cards}
                </div>
            </div>
        </div>
    )
}

export const getServerSideProps = async (pageContext) => {
    const pageSlug = pageContext.query.name
    const query = `*[title match "${pageSlug}*"]{
        title,
        _createdAt,
        author->{
            _id,
            name,
            image
        },
        "categories": categories[]->title,
        mainImage,
        body,
        keyword,
        slug
      }`
    
      const property = await sanityClient.fetch(query, { pageSlug })
    
      if (!property) {
        return {
          props: null,
          notFound: true,
        }
      } else {
        return {
          props: {
            property
          },
        }
      }
  }


export default Posts
