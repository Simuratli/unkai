import Navbar from '../components/Navbar/Navbar'
import React,{useEffect,useState} from 'react'
import Footer from '../components/Footer/Footer'
import '../styles/globals.css'
import Head from 'next/head'
import { CategoryProvider } from "../context/categoryContext";


function MyApp({ Component, pageProps }) {


  return(
    <CategoryProvider>
    <div>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <Navbar/>
      <Component {...pageProps} />
    </div>
    <Footer/>
  </CategoryProvider>
  ) 
  
}

export default MyApp
