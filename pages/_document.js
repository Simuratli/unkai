import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class Pwa extends  Document {

static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }
    render() {
        return (
            <Html amp>
                <Head>
                <link rel="manifest" href="/manifest.json" />
                <link rel="apple-touch-icon" href="/vercel.png" />
                <meta name="theme-color" content='#fff' />
                </Head>
                <body>
                    <Main/>
                    <NextScript/>
                    <script
                        async
                        src="https://www.googletagmanager.com/gtag/js?id=G-03ZEDWETFF"
                    />

                    <script
                        dangerouslySetInnerHTML={{
                        __html: `window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());

                        gtag('config', 'G-03ZEDWETFF');`
                        }}
                    />


                   

                </body>
            </Html>
        );
    }
}
