import React from 'react'
import {sanityClient,PortableText,urlFor} from '../../sanity'
import Head from 'next/head'
import { useRouter } from 'next/router'
import {
    EmailShareButton,
    FacebookShareButton,
    LinkedinShareButton,
    TelegramShareButton,
    TwitterShareButton,
  } from "react-share";

function Post(props) {
    const router = useRouter()
    const { slug } = router.query
    let date = new Date(props.property._createdAt)
    let month = date.getMonth()
    let day = date.getDay()
    let year = date.getFullYear()
    let months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
        ];

    return (
        <div className='container'>
        <Head>
            <title>{props.property.title}</title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta charSet="UTF-8"/>
            <meta name="description" content={<PortableText blocks={props.property.body} />}/>
            <meta name="keywords" content={`HTML, CSS, JavaScript, Unkai 404 , game development , topics about different technologies , Unity , Unkai studio, Japan game studio , Azerbaijan game studio, Azerbaijan tech , tech , game studio ${props.property.keyword.join(' , ')}`} />
            <meta name="author" content={`Eljan Simuratli , ${props.property.author.name}`}/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        
        </Head>
        <Head>
            <meta property="og:title" content={props.property.title}/>
            <meta property="og:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="og:image" content={props.property.mainImage ? urlFor(props.property.mainImage) : 'https://i.resimyukle.xyz/fCdM45.png'}  />
            <meta property="og:url" content={`http://unkai.vercel.app/post/${slug}`}/>
            <meta name="twitter:title" content={props.property.title} />
            <meta name="twitter:description" content={<PortableText blocks={props.property.body} />}/>
            <meta name="twitter:image" content={props.property.mainImage ? urlFor(props.property.mainImage) : 'https://i.resimyukle.xyz/fCdM45.png'}  />
        </Head>
            <div className='post'>
                <span className='post-category'>{props.property.categories.join(' , ')}</span>
                <h1 className='post-head'>{props.property.title}</h1>
                <p className='post-author'>{months[month]} {day}.{year} | {props.property.author.name}</p>
                {props.property.mainImage && 
                <img className='post-img' alt={props.property.title} src={urlFor(props.property.mainImage)} />
                }
                <div className='post-content'>
                <PortableText blocks={props.property.body} />
                </div>
                <div className='post-tags'>
                {props.property.keyword.map((item,index)=>{
                    return <span key={index} className='hashtag'>#{item}</span>
                })}
                </div>
                <div className='post-share'>
                    <div className='share'>
                        <div>Share</div>
                        <FacebookShareButton url={`https://unkai.vercel.app/post/${slug}`}>
                            <button className='share-img'><img alt='facebook' src='/assets/icons/facebook.png' /></button>
                        </FacebookShareButton>
                        <LinkedinShareButton url={`https://unkai.vercel.app/post/${slug}`}>
                            <button className='share-img'><img alt='linkedin' src='/assets/icons/linkedin.png' /></button>
                        </LinkedinShareButton>
                        <TwitterShareButton url={`https://unkai.vercel.app/post/${slug}`}>
                            <button className='share-img'><img alt='twitter' src='/assets/icons/twitter.png' /></button>
                        </TwitterShareButton>
                        <EmailShareButton url={`https://unkai.vercel.app/post/${slug}`}>
                            <button className='share-img'><img alt='email' src='/assets/icons/email.png' /></button>
                        </EmailShareButton>
                        <TelegramShareButton url={`https://unkai.vercel.app/post/${slug}`}>
                            <button className='share-img'><img alt='telegram' src='/assets/icons/telegram.png' /></button>
                        </TelegramShareButton>
                    </div>
                </div>

            </div>
        </div> 
    )
}




export const getServerSideProps = async (pageContext) => {
    const pageSlug = pageContext.query.slug
    const query = `*[ _type == "post" && slug.current == $pageSlug][0]{
        title,
        _createdAt,
        author->{
            _id,
            name,
            image
        },
        "categories": categories[]->title,
        mainImage,
        body,
        keyword
      }`
    
      const property = await sanityClient.fetch(query, { pageSlug })
    
      if (!property) {
        return {
          props: null,
          notFound: true,
        }
      } else {
        return {
          props: {
            property
          },
        }
      }
  }

export default Post