import Head from 'next/head'
import Heading from "../components/Heading/Heading";
import BlogCard from '../components/BlogCard/BlogCard'
import {sanityClient} from '../sanity'
import {CategoryContext} from '../context/categoryContext.js'
import {useContext} from 'react'
import Loader from '../components/Loader'

function Home(props) {

  const [context, setContext] = useContext(CategoryContext)
  let CARDS = context.posts && context.posts.map((item,index)=>{
    return <BlogCard data={item} key={index} />
  })

  if(context.loading) CARDS = <Loader/>

  return (
    <div>
      <Head>
            <title>Unkai | Development Blog </title>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta charSet="UTF-8"/>
            <meta name="description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="keywords" content="HTML, CSS, JavaScript, Unkai 404 , game development , topics about different technologies , Unity , Unkai studio, Japan game studio , Azerbaijan game studio, Azerbaijan tech , tech , game studio"/>
            <meta name="author" content="Eljan Simuratli"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </Head>
        <Head>
            <meta property="og:title" content="Unkai | Development Blog"/>
            <meta property="og:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta property="og:image" content="https://i.resimyukle.xyz/ezP6Q9.png"/>
            <meta property="og:url" content="http://unkai.vercel.app/blog"/>
            <meta name="twitter:title" content="Unkai | Development Blog"/>
            <meta name="twitter:description" content="Unkai is an open platform where you can read inspiring articles on different technologies, especially front end and game development, and on different topics."/>
            <meta name="twitter:image" content="https://i.resimyukle.xyz/ezP6Q9.png"/>
        </Head>

      <main className='container main_content'>
        <div className='container'>
          <Heading category={props.category} />
          
          {CARDS}

        </div>
      </main>
      
    </div>
  )
}


export default Home